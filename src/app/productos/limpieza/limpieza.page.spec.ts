import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LimpiezaPage } from './limpieza.page';

describe('LimpiezaPage', () => {
  let component: LimpiezaPage;
  let fixture: ComponentFixture<LimpiezaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LimpiezaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LimpiezaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
