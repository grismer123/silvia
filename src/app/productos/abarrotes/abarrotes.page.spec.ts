import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AbarrotesPage } from './abarrotes.page';

describe('AbarrotesPage', () => {
  let component: AbarrotesPage;
  let fixture: ComponentFixture<AbarrotesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbarrotesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AbarrotesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
