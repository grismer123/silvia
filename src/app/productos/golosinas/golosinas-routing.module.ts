import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GolosinasPage } from './golosinas.page';

const routes: Routes = [
  {
    path: '',
    component: GolosinasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GolosinasPageRoutingModule {}
