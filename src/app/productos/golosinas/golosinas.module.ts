import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GolosinasPageRoutingModule } from './golosinas-routing.module';

import { GolosinasPage } from './golosinas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GolosinasPageRoutingModule
  ],
  declarations: [GolosinasPage]
})
export class GolosinasPageModule {}
