import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GolosinasPage } from './golosinas.page';

describe('GolosinasPage', () => {
  let component: GolosinasPage;
  let fixture: ComponentFixture<GolosinasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GolosinasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GolosinasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
