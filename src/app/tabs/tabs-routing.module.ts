import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:[
      {
        path:'home',
        children:[
          {
            path:'',
            loadChildren:() => import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },

      {
        path:'login',
        children:[
          {
            path:'',
            loadChildren:() => import('../login/login.module').then(m => m.LoginPageModule)
          }
        ]
      },
      {
        path:'abarrotes',
        children:[
          {
            path:'',
            loadChildren:() => import('../productos/abarrotes/abarrotes.module').then(m => m.AbarrotesPageModule)
          }
        ]
      },
      {
        path:'bebidas',
        children:[
          {
            path:'',
            loadChildren:() => import('../productos/bebidas/bebidas.module').then(m => m.BebidasPageModule)
          }
        ]
      },
      {
        path:'golosinas',
        children:[
          {
            path:'',
            loadChildren:() => import('../productos/golosinas/golosinas.module').then(m => m.GolosinasPageModule)
          }
        ]
      },
      {
        path:'limpieza',
        children:[
          {
            path:'',
            loadChildren:() => import('../productos/limpieza/limpieza.module').then(m => m.LimpiezaPageModule)
          }
        ]
      },
      {
        path:'mascotas',
        children:[
          {
            path:'',
            loadChildren:() => import('../productos/mascotas/mascotas.module').then(m => m.MascotasPageModule)
          }
        ]
      },
      {
        path:'verdura',
        children:[
          {
            path:'',
            loadChildren:() => import('../productos/verduras/verduras.module').then(m => m.VerdurasPageModule)
          }
        ]
      },
      {
        path:'perfil',
        children:[
          {
            path:'',
            loadChildren:() => import('../perfil/perfil.module').then(m => m.PerfilPageModule)
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
/*
{
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
*/