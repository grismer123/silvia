import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Reg1Page } from './reg1.page';

const routes: Routes = [
  {
    path: '',
    component: Reg1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Reg1PageRoutingModule {}
