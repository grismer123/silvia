import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Reg1Page } from './reg1.page';

describe('Reg1Page', () => {
  let component: Reg1Page;
  let fixture: ComponentFixture<Reg1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Reg1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Reg1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
