import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Reg1PageRoutingModule } from './reg1-routing.module';

import { Reg1Page } from './reg1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Reg1PageRoutingModule
  ],
  declarations: [Reg1Page]
})
export class Reg1PageModule {}
