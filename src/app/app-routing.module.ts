import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'reg1',
    loadChildren: () => import('./registro/reg1/reg1.module').then( m => m.Reg1PageModule)
  },
  {
    path: 'reg2',
    loadChildren: () => import('./registro/reg2/reg2.module').then( m => m.Reg2PageModule)
  },
  {
    path: 'abarrotes',
    loadChildren: () => import('./productos/abarrotes/abarrotes.module').then( m => m.AbarrotesPageModule)
  },
  {
    path: 'bebidas',
    loadChildren: () => import('./productos/bebidas/bebidas.module').then( m => m.BebidasPageModule)
  },
  {
    path: 'golosinas',
    loadChildren: () => import('./productos/golosinas/golosinas.module').then( m => m.GolosinasPageModule)
  },
  {
    path: 'verduras',
    loadChildren: () => import('./productos/verduras/verduras.module').then( m => m.VerdurasPageModule)
  },
  {
    path: 'mascotas',
    loadChildren: () => import('./productos/mascotas/mascotas.module').then( m => m.MascotasPageModule)
  },
  {
    path: 'limpieza',
    loadChildren: () => import('./productos/limpieza/limpieza.module').then( m => m.LimpiezaPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)
  },  {
    path: 'perfil',
    loadChildren: () => import('./perfil/perfil.module').then( m => m.PerfilPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
